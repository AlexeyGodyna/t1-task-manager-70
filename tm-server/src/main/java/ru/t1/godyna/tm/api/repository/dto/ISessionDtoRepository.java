package ru.t1.godyna.tm.api.repository.dto;

import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.godyna.tm.dto.model.SessionDTO;

@Repository
@Scope("prototype")
public interface ISessionDtoRepository extends IDtoRepository<SessionDTO> {

    @Transactional
    void deleteByUserId(String userId);

    @Nullable
    SessionDTO findByUserIdAndId(String userId, String id);

}
